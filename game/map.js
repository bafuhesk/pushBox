/*操作地图*/

//游戏主容器
var con=document.getElementById("content");

//所有地图（关卡）
var maps=[];

//当前地图
var map=[];

//当前关卡
var level=0;//当前关卡

var w=10;//地图宽度
var h=10;//地图高度

/*地图代号*/
var empty=0;//空地
var wall=1;//墙
var end=2;//目的地
var box=3;//箱子
var player=4;//玩家
var pe=6;//玩家+目的地
var done=5;//箱子+目的地

/*地图元素*/
//空地
var con_empty="<span class='empty'></span>";
//墙
var con_wall="<span class='wall'></span>";
//目的地
var con_end="<span class='end'></span>";
//箱子
var con_box="<span class='box'></span>";
//玩家
var con_player="<span class='player'></span>";
//玩家+目的地
var con_pe="<span class='pe'></span>";
//箱子+目的地
var con_done="<span class='done'></span>";

/**
 * 加载地图
 */
function getMap() {
    con.innerHTML="";
    for (var i=0;i<h;i++){
        for (var j=0;j<w;j++){
            switch (map[i][j]){
                case empty:con.innerHTML+=con_empty;
                    break;
                case wall:con.innerHTML+=con_wall;
                    break;
                case end:con.innerHTML+=con_end;
                    break;
                case box:con.innerHTML+=con_box;
                    break
                case player:con.innerHTML+=con_player;
                    break;
                case pe:con.innerHTML+=con_pe;
                    break;
                case done:con.innerHTML+=con_done;
                    break;

            }
        }
        con.innerHTML+='<p></p>';
    }
}

/**
 * 获取玩家当前坐标
 */
function getPlayerLocation(){
    for (var i=0;i<h;i++){
        for (var j=0;j<w;j++){
            if(map[i][j]==player||map[i][j]==pe){
                x=i;
                y=j;
            }
        }
    }
}

/**
 * 设置当前地图
 * @param i
 * @returns {Array}
 */
function setMap(i) {
    map=copy(maps[i]);//当地地图
    w=map[0].length;//地图宽度
    h=map.length;//地图高度
}


//--------------------两种初始化方式---------------------//

/**
 * json方式初始化
 */
function initJson() {
    getJson("map.json",function (data) {
        maps=data;
        setMap(level);
        getMap();
    });
}

/**
 * js方式初始化
 */
function initJs() {
    maps=levels;
    setMap(level);
    getMap();
}

/**
 * 初始化
 */
(function(){

    // initJson();//json方式初始化
    initJs();//js方式初始化

})()

